const express = require("express");
const app = express();
const port = 3000; //port listen
const routes = require("./routes/router");
const Middleware = require("./middleware/middle");

// ejs
app.use(express.static("views/assets"));
app.set("view engine", "ejs");

// mem-parsing permintaan masuk dengan muatan JSON dan didasarkan pada body-parser .
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.use(routes);
app.use(Middleware.errHandler);

//unknown route error
app.all("*", (req, res) => {
  res.status(404).end();
});

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`);
});
