class Middleware {
  static middle(req, res, next) {
    console.log("==BAGIAN MIDDLEWARE==");
    console.log(req.body);
    const name = req.body.name;
    next();
  }
  //  Error Handler
  static errHandler(err, req, res, next) {
    console.log("==Error handler==");
    if (err.status) {
      const _status = err.status;
      res.status(_status).json(err);
    } else {
      res.status(500).json({
        messages: "Hayulu moment",
      });
      res.render("error", { error: err });
    }
    // if (res.headersSent) {
    //   return next(err);
    // } else {
    //   res.status(500);
    //   res.render("error", { error: err });
    //   next();
    // }
  }
}

module.exports = Middleware;
