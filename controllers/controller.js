const User = require("./user"); //connect user ke main index
class Controller {
  // View
  static HomeView(req, res, next) {
    // res.send("");
    res.render("index.ejs");
  }

  static Viewch3(req, res, next) {
    // res.send("ch3");
    console.log("==ch3==");
    res.render("ch3");
  }
  static Viewch4(req, res, next) {
    // res.send("ch4");
    console.log("==ch4==");
    res.render("ch4");
  }

  // Models
  static getHome(req, res, next) {
    console.log("=== Controller ===");
    try {
      res.send("fungsi GET HOME berhasil");
      // error test
      // const _status = err.status;
      // res.status(_status).json(err);
    } catch (error) {
      console.log(error, "<<<<");
      next(error);
    }
  }
  static postHome(req, res, next) {
    console.log("=== Controller ===");
    try {
      // error test
      // const _status = err.status;
      // res.status(_status).json(err);
      res.send("fungsi CREATE/POST HOME berhasil");
    } catch (error) {
      console.log(error, "<<<<");
      next(error);
    }
  }
  static editHome(req, res, next) {
    console.log("=== Controller ===");
    try {
      res.send("fungsi UPDATE/PUT HOME berhasil");
      // error test
      // const _status = err.status;
      // res.status(_status).json(err);
    } catch (error) {
      console.log(error, "<<<<");
      next(error);
    }
  }
  static deleteHome(req, res, next) {
    console.log("=== Controller ===");
    try {
      res.send("fungsi DELETE HOME berhasil");
      // error test
      // const _status = err.status;
      // res.status(_status).json(err);
    } catch (error) {
      console.log(error, "<<<<");
      next(error);
    }
  }
}

module.exports = { Controller, User };
