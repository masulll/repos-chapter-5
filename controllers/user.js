// const user = require("../models/users.json");
const { deleteuserid } = require("../models/Modeluser");
const Modeluser = require("../models/Modeluser");
class User {
  static getuser(req, res, next) {
    try {
      const _user = Modeluser.getuser(); //protected
      res.status(200).json(_user);
    } catch (error) {
      next(error);
    }
  }

  static getuserid(req, res, next) {
    try {
      const { id } = req.params;
      const userid = Modeluser.getuserid(id);
      res.status(200).json(userid);
    } catch (error) {
      next(error);
    }
  }

  static deleteuserid(req, res, next) {
    try {
      const { id } = req.params;
      const userid = Modeluser.deleteuserid(id);
      res.status(200).json({ id: deleteuserid });
    } catch (error) {
      next(error);
    }
  }
}

module.exports = User;
