const user = require("./users.json");

class Modeluser {
  static getuser() {
    const _user = user;
    _user.forEach((el) => {
      delete el.password; // temporary delete
    });

    return _user;
  }

  static getuserid(id) {
    const _user = user;
    const idx = _user.findIndex((x) => x.id == id);
    const finaldata = _user[idx];

    delete finaldata.password;
    return finaldata;
  }

  // soft delete
  static deleteuserid(id) {
    const _user = user;
    const idx = _user.findIndex((x) => x.id == id);
    _user.splice(idx, 1);
    return id;
  }
}

module.exports = Modeluser;
