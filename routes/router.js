const router = require("express").Router();
const { Controller, User } = require("../controllers/controller");
const Middleware = require("../middleware/middle");

// Home Route
router.get("/", Controller.HomeView);
router.get("/ch3", Middleware.middle, Controller.Viewch3); //view with middleware
router.get("/ch4", Controller.Viewch4); //view without middleware
router.get("/home", Middleware.middle, Controller.getHome);
router.post("/", Middleware.middle, Controller.postHome);
router.put("/", Middleware.middle, Controller.editHome);
router.delete("/", Middleware.middle, Controller.deleteHome);

// User Route
router.get("/user", User.getuser);
router.get("/user/:id", User.getuserid);
router.delete("/user/:id", User.deleteuserid);
module.exports = router;
